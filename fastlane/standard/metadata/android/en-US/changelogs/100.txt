New in 3.1.5
★ Update some translations.

New in 3.1.4
★ WaveUp ✌️, 🍻, ❤, and 💵 violate the Google Play policy due to
  a "Violation of Spam policy" due to "duplicative content". This is
  an attempt to try to warn the users to download the normal version.

  They will be unpublished in a couple of days! Users of paid versions
  please download the standard version if you wish to continue getting
  updates.

  Sorry for the inconvenience.

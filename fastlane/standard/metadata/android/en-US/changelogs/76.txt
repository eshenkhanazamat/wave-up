Dear Android 9 users, if you had hidden the notification, you might have to hide it again. Due to a big Android change (not allowing apps to read sensors in the background) has forced WaveUp to always show a notification. Hopefully, it'll still work if you hide it with the new setting.

New in 2.6.6 (a and b too)
★ Update Italian translation.

New in 2.6.5
★ Release WaveUp :v: (peace) and :beers: (cheers).

New in 2.6.4
★ Do not disable WaveUp during bluetooth or "jack" calls.